package com.example.maptechnology.manutencaoapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.maptechnology.manutencaoapp.R;

public class PrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }
}
