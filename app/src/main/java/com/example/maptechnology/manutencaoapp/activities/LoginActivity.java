package com.example.maptechnology.manutencaoapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.maptechnology.manutencaoapp.R;
import com.example.maptechnology.manutencaoapp.rest.RetrofitClass;
import com.example.maptechnology.manutencaoapp.models.Usuario;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    EditText matricula;
    EditText senha;
    Button entrar;

    final String url = "http://192.168.0.14:8000/";
    RetrofitClass apiService;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.pref_key), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiService = retrofit.create(RetrofitClass.class);


        matricula = (EditText) findViewById(R.id.editMatricula);
        senha = (EditText) findViewById(R.id.editSenha);

        entrar = (Button) findViewById(R.id.btnEntrar);
        entrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view == entrar){

            Call<Usuario> call2 = apiService.logar(matricula.getText().toString(),senha.getText().toString());
            call2.enqueue(new Callback<Usuario>() {

                @Override
                public void onResponse(Call<Usuario> call, retrofit2.Response<Usuario> response) {
                    int statusCode = response.code();

                    Usuario usuario = response.body();

                    if (response.message().equals("OK")) {

                        editor.putString( getString(R.string.matricula), usuario.getUserData().getEmail());
                        editor.putString( getString(R.string.hierarquia), usuario.getUser().getHierarquia());
                        editor.putString( getString(R.string.horahomen), usuario.getUser().getHomemHora());
                        editor.putInt(getString(R.string.statusLogin),1 );
                        editor.commit();

                        Intent i = new Intent(getApplicationContext(),PrincipalActivity.class);
                        startActivity(i);

                        Toast.makeText(getApplicationContext(),"Seja Bem Vindo(a) " + usuario.getUser().getNome(),Toast.LENGTH_SHORT).show();

    

                    }else{

                        Toast.makeText(getApplicationContext(),"Erro de Login. Verifique Matrícula e Senha e tente outra vez.",Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Usuario> call, Throwable t) {
                    // Log error here since request failed
                    Log.d("error", t.toString());
                }
            });


        }

    }
}
