package com.example.maptechnology.manutencaoapp.rest;

import com.example.maptechnology.manutencaoapp.models.Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by MAPTECHNOLOGY on 22/02/2019.
 */

public interface RetrofitClass {

    @FormUrlEncoded
    @POST("usuario/criar/")
    Call<Usuario> cadastraUsuario(@Field("nome") String nome ,
                               @Field("dataCriacao") String dataCriacao,
                               @Field("matricula") String matricula,
                               @Field("senha") String senha,
                               @Field("hierarquia") String hierarquia,
                               @Field("status") String status,
                               @Field("valorhh") String valorhh

    );

    @FormUrlEncoded
    @POST("usuario/login/")
    Call<Usuario> logar(@Field("matricula") String matricula ,
                                  @Field("senha") String senha


    );
}
